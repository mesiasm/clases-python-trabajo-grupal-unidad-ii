"""                author: "Angel Morocho"  "Valeria Guerrero" "Karen Paladines"
             email: "angel.m.morocho.c@unl.edu.ec" "valeria.guerrero@unl.edu.ec" "karen.paladines@unl.edu.ec"

Ejercicio 1:
            Elabore tres clases con codigo Python, con el tema de su recurso educativo Grupal"""


class geometria:
    """Define el area de un polígono según su base y la altura"""
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura


class Rectangulo(geometria):

    def area(self):
        area = self.base * self.altura
        return area

    def __str__(self):
        return f"mul {self.area}"


class Triangulo(geometria):

    def area(self):
        area = (self.base * self.altura) / 2
        return area

    def __str__(self):
        return f"mul {self.area}"


rectangulo = Rectangulo(14, 12)  # Estos son valores refernciales para comprobar el ejercicio
triangulo = Triangulo(5, 11)  # Estos son valores refernciales para comprobar el ejercicio

print("Área del rectángulo: ", rectangulo.area())
print("Área del triángulo:", triangulo.area())